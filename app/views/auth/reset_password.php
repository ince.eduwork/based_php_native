<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                        <div class="login-brand">
                            <a href="<?= BASEURL ?>">
                                <img src="<?= BASEURL ?>/public/assets/img/logo/logo.png" alt="logo" width="200">
                            </a>
                        </div>

                        <div class="card card-main">
                            <div class="card-header">
                                <h4>Reset password</h4>
                            </div>

                            <div class="card-body">
                                <form action="<?= BASEURL ?>/auth/post_reset_password" method="POST">
                                    <?= FlashMessage::showFlash(); ?>
                                    <div class="form-group">
                                        <input required type="hidden" class="form-control" name="token" value="<?= $data['token'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">email</label>
                                        <input required type="text" class="form-control" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="password_baru">password_baru</label>
                                        <input required type="text" class="form-control" name="password_baru">
                                    </div>
                                    <div class="form-group">
                                        <label for="konfirmasi_password_baru">konfirmasi_password_baru</label>
                                        <input required type="text" class="form-control" name="konfirmasi_password_baru">
                                    </div>
                                    <div class="form-group">
                                        <button name="reset" type="submit" class="btn bg-main text-white btn-lg btn-block" tabindex="4">
                                            reset password
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <div class="simple-footer">
                            Copyright &copy; samtam 2023
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


</body>

</html>