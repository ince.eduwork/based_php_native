<?php

class Auth extends Controller
{
    public function __construct()
    {
        if (isset($_SESSION['login'])) {
            header('location: ' . BASEURL);
        }
    }

    public function index()
    {
        if (isset($_POST['signIn'])) {
            if ($this->model('AuthModel')->prosesLogin($_POST) == true) {
                header('location: ' . BASEURL . '/dashboard');
            } else {
                // echo "gagal";
            }
        }
        $this->view('auth_templates/header');
        $this->view('auth/index');
    }

    public function link_reset_password()
    {
        if (isset($_POST['reset'])) {
            if ($this->model('AuthModel')->linkResetPassword($_POST) == true) {
                header('location: ' . BASEURL . '/auth/link_reset_password');
            } else {
                // echo "gagal";
            }
        }
        $this->view('auth_templates/header');
        $this->view('auth/link_reset_password');
    }

    public function reset_password($token)
    {
        $tokenCheck = $this->model('AuthModel')->cekToken($token);

        if ($tokenCheck == 'expired') {
            FlashMessage::setFlash("token reset telah expired", 'red');
            header('location:' . BASEURL . '/auth');
        } else if ($tokenCheck == 'invalid') {
            FlashMessage::setFlash("token and invalid", 'red');
            header('location:' . BASEURL . '/auth');
        } else if ($tokenCheck = 'valid') {
            $data['token'] = $token;
            $this->view('auth_templates/header');
            $this->view('auth/reset_password', $data);
        }
    }

    public function post_reset_password()
    {
        $resetPass = $this->model('AuthModel')->resetPassword($_POST);
        if ($resetPass == 'invalid_email') {
            FlashMessage::setFlash("email tidak dikenali", 'red');
            Helpers::redirectBack();
        } else if ($resetPass == 'pass_not_match') {
            FlashMessage::setFlash("passsword tidak match", 'coral');
            Helpers::redirectBack();
        } else if ($resetPass == 'illegal_email') {
            FlashMessage::setFlash("reset password ini bukan untuk email yang anda masukkan", 'coral');
            Helpers::redirectBack();
        } else {
            FlashMessage::setFlash("Pass berhasil di reset", 'green');
            header('location:' . BASEURL . '/auth');
        }
    }

    public function logout()
    {
        session_destroy();
        header('location:' . BASEURL . '/auth');
    }
}
