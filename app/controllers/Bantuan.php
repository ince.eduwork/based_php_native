<?php

class Bantuan extends Controller
{
    public function __construct()
    {
        LoginCheck::isLogin();
    }

    public function index()
    {
        $data['judul'] = 'bantuan';
        $data['liClassActive'] = 'liBantuan';
        $this->view('templates/header', $data);
        $this->view('templates/navbar');
        $this->view('templates/sidebar');
        $this->view('pages/bantuan/index');
        $this->view('templates/footer', $data);
    }
}
