<?php

class Dashboard extends Controller
{

    public function __construct()
    {
        RoleCheck::cekLevel(['admin']);
        LoginCheck::isLogin();
    }

    public function index()
    {
        $data['judul'] = 'dashboard';
        $data['liClassActive'] = 'liDashboard';
        $this->view('templates/header', $data);
        $this->view('templates/navbar');
        $this->view('templates/sidebar');
        $this->view('pages/dashboard/index');
        $this->view('templates/footer', $data);
    }
}
