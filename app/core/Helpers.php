<?php

class Helpers
{
    public static function setAlert($pesan)
    {
        $_SESSION['alert'] = [
            'pesan' => $pesan,
        ];
    }

    public static function showAlert()
    {
        if (isset($_SESSION['alert'])) {
            echo "<script>
            document.addEventListener('DOMContentLoaded', function() {
                toastr.info(
                    '" . $_SESSION['alert']['pesan'] . "',
                );
            });
            </script>";
            unset($_SESSION['alert']);
        }
    }

    public static function redirectBack()
    {
        header("Location: {$_SERVER['HTTP_REFERER']}"); //redirect back
    }
}
