<?php

class FlashMessage
{
    public static function setFlash($pesan, $color)
    {
        $_SESSION['flash'] = [
            'pesan' => $pesan,
            'color' => $color,
        ];
    }

    public static function showFlash()
    {
        if (isset($_SESSION['flash'])) {
            echo "<p style=color:" . $_SESSION['flash']['color'] . ";>" . $_SESSION['flash']['pesan'] . "</p>";
            unset($_SESSION['flash']);
        }
    }
}
