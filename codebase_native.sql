-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2021 at 02:34 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `si_alterga`
--

-- --------------------------------------------------------

--
-- Table structure for table `costumer`
--

CREATE TABLE `costumer` (
  `costumer_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_hp` char(20) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jenis_perbaikan_id` int(11) NOT NULL,
  `kondisi_servisan` varchar(100) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `jenis_pelanggan` varchar(50) NOT NULL,
  `laptop_id` int(11) NOT NULL,
  `detail_masalah` varchar(150) NOT NULL,
  `estimasi_biaya` varchar(100) NOT NULL,
  `teknisi` varchar(100) NOT NULL,
  `histori_pengerjaan` varchar(100) NOT NULL,
  `tgl_masuk` int(11) NOT NULL,
  `tgl_keluar` int(11) NOT NULL,
  `soft_delete` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `costumer`
--

INSERT INTO `costumer` (`costumer_id`, `nama`, `no_hp`, `alamat`, `jenis_perbaikan_id`, `kondisi_servisan`, `keterangan`, `jenis_pelanggan`, `laptop_id`, `detail_masalah`, `estimasi_biaya`, `teknisi`, `histori_pengerjaan`, `tgl_masuk`, `tgl_keluar`, `soft_delete`) VALUES
(731, 'SAHRUL', '085255005069', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 1, 'ganti keyboard', '0', 'none', 'none', 1578092400, 0, 1),
(732, 'HATTA', '085399242037', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 2, 'Install Windows 10', '0', 'none', 'none', 1598911200, 1598911200, 1),
(733, 'IRFAN', '085299170796', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 3, 'Matol', '0', 'none', 'none', 1598911200, 1607641200, 1),
(734, 'SAMATOR GAS', '081242030604', 'Makassar', 10, 'none', 'none', 'TOKO / TEKNISI', 4, 'Pesan PSU Dell Sdah DP: 500.000', '0', 'none', 'none', 1598911200, 1599688800, 1),
(735, 'RIDHO', '082293454099', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 5, 'Install Windows 7', '0', 'none', 'none', 1598997600, 1599084000, 1),
(736, 'SAMATOR', '081242030604', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 6, 'cek? Matol', '0', 'none', 'none', 1598997600, 1600293600, 1),
(737, 'MUH RIDWAN', '085217494888', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 7, 'install windows 10', '0', 'none', 'none', 1599084000, 1599084000, 1),
(738, 'RAFLI', '081319233861', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 8, 'install windows 10', '0', 'none', 'none', 1599084000, 1599688800, 1),
(739, 'UCA', '085340552559', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 9, 'matol', '0', 'none', 'none', 1599084000, 1600552800, 1),
(740, 'UCA', '085340552559', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 10, 'matol', '0', 'none', 'none', 1599084000, 1600552800, 1),
(741, 'ALI', '082296903645', 'Makassar', 7, 'none', 'none', 'PELANGGAN', 11, 'Pesan Baterai', '0', 'none', 'none', 1599170400, 1599688800, 1),
(742, 'PUTRI', '085394304387', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 12, 'install windows 10', '0', 'none', 'none', 1599170400, 0, 1),
(743, 'REZKI', '085284423076', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 13, 'Ganti Kipas', '0', 'none', 'none', 1599170400, 1599688800, 1),
(744, 'REZKI', '085284423076', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 14, 'Ram yang terpasang tidak sesuai pas masuk', '0', 'none', 'none', 1599170400, 1599688800, 1),
(745, 'DIANA', '0821189182298', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 15, 'install windows 10', '0', 'none', 'none', 1599256800, 1599688800, 1),
(746, 'ZAIN', '081342505635', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 16, 'matol', '0', 'none', 'none', 1599256800, 0, 1),
(747, 'TAMRAN', '085341000338', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 17, 'Install Windows 10', '0', 'none', 'none', 1599256800, 1599688800, 1),
(748, 'NUNUNG', '085240228044', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 18, 'no display', '0', 'none', 'none', 1599256800, 1599948000, 1),
(749, 'KAKAM', '081355530575', 'Makassar', 7, 'none', 'none', 'TOKO / TEKNISI', 19, 'Pesan Baterai Sudah dp 50000', '0', 'none', 'none', 1599256800, 0, 1),
(750, 'SIDIK', '082350670421', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 20, 'no display', '0', 'none', 'none', 1599343200, 1604358000, 1),
(751, 'FATUR', '081342188620', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 21, 'cek? Tampil gelap', '0', 'none', 'none', 1599343200, 1599688800, 1),
(752, 'SAMSUL', '082190909103', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 22, 'install windows 10', '0', 'none', 'none', 1599429600, 1599688800, 1),
(753, 'ANDI', '085298901403', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 23, 'install windows 10', '0', 'none', 'none', 1599429600, 1599688800, 1),
(754, 'VIOLA', '085255248788', 'Makassar', 2, 'none', 'none', 'PELANGGAN', 24, 'servis engsel kanan', '0', 'none', 'none', 1599429600, 1599516000, 1),
(755, 'RIDWAN', '08111161135', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 25, 'matol', '0', 'none', 'none', 1599429600, 1600552800, 1),
(756, 'RONI', '082189016665', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 26, 'matol', '0', 'none', 'none', 1599516000, 1600552800, 1),
(757, 'AMI KOMPUTER', '085255986412', 'Makassar', 10, 'none', 'none', 'TOKO / TEKNISI', 27, 'lunas 450000', '0', 'none', 'none', 1599516000, 0, 1),
(758, 'ASRI', '085396441195', 'Makassar', 7, 'none', 'none', 'PELANGGAN', 28, 'Pesan Baterai Sudah dp 100000', '0', 'none', 'none', 1599516000, 1599775200, 1),
(759, 'AWAL', '082345612100', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 29, 'install windows 10 ', '0', 'none', 'none', 1599602400, 1599688800, 1),
(760, 'NURLAILA', '085205622344', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 30, 'install filmora dan zoom', '0', 'none', 'none', 1599602400, 1599688800, 1),
(761, 'RIVAN', '081245523899', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 31, 'mati-mati', '0', 'none', 'none', 1599602400, 1600725600, 1),
(762, 'REZKI', '085284423076', 'Makassar', 5, 'none', 'none', 'PELANGGAN', 32, 'ganti lcd konfirmasi langsung', '0', 'none', 'none', 1599602400, 1599688800, 1),
(763, 'WAHYU', '081394703942', 'Makassar', 8, 'none', 'none', 'PELANGGAN', 33, 'HDD tidak terbaca', '0', 'none', 'none', 1599602400, 0, 1),
(764, 'SANDRA', '081243344210', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 34, 'cek masuk bios', '0', 'none', 'none', 1599688800, 1599775200, 1),
(765, 'LEO', '085211010888', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 35, 'cek matol', '0', 'none', 'none', 1599688800, 1600293600, 1),
(766, 'BUR', '081241887303', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 36, 'cek speaker', '0', 'none', 'none', 1599775200, 1599775200, 1),
(767, 'WIWI', '081242156889', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 37, 'install windows 10', '0', 'none', 'none', 1599775200, 1599775200, 1),
(768, 'CHANDRA', '087716742235', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 38, 'cek ? Blank putih', '0', 'none', 'none', 1599775200, 1600120800, 1),
(769, 'IAN', '085342676014', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 39, 'power tidak tahan garansi 31-8-2020', '0', 'none', 'none', 1599775200, 1601330400, 1),
(770, 'WULAN', '082348773745', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 40, 'install windows 10 wifi tidak mau conect', '0', 'none', 'none', 1599775200, 1600120800, 1),
(771, 'UCA', '085340552559', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 41, 'no display Garansi (24 jun 2020)', '0', 'none', 'none', 1599775200, 1603058400, 1),
(772, 'NUR FADILA', '085825320138', 'Makassar', 7, 'none', 'none', 'PELANGGAN', 42, 'intstall windows 10', '0', 'none', 'none', 1599861600, 1599948000, 1),
(773, 'FAJAR', '082292649975', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 43, 'no display', '0', 'none', 'none', 1600034400, 0, 1),
(774, 'ARFIAN', '085255408805', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 44, 'install windows 10', '0', 'none', 'none', 1600034400, 1600120800, 1),
(775, 'GALIH', '085255571515', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 45, 'install windows 7', '0', 'none', 'none', 1600034400, 1600120800, 1),
(776, 'ILMI', '081241439401', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 46, 'matol', '0', 'none', 'none', 1600034400, 1602799200, 1),
(777, 'AMRIADAI', '085211015505', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 47, 'matol', '0', 'none', 'none', 1600034400, 1600552800, 1),
(778, 'AMRIADAI', '085211015505', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 48, 'none', '0', 'none', 'none', 1600034400, 0, 1),
(779, 'ERMY SOLAH', '081343218485', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 49, 'cek kipas', '0', 'none', 'none', 1600120800, 1600725600, 1),
(780, 'NUR', '082323337753', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 50, 'install windows 7', '0', 'none', 'none', 1600120800, 1600293600, 1),
(781, 'SITI HAYATI', '085242929657', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 51, 'no display', '0', 'none', 'none', 1600120800, 1600293600, 1),
(782, 'SITI HAYATI', '085242929657', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 52, 'pesan keyboard Dp 100000', '0', 'none', 'none', 1600120800, 1600293600, 1),
(783, 'ADI', '085813028961', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 53, 'install windows 7', '0', 'none', 'none', 1600207200, 1600293600, 1),
(784, 'ALAM', '081341549907', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 54, 'matol', '0', 'none', 'none', 1600207200, 1601589600, 1),
(785, 'ERWIN', '085396935343', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 55, 'install windows 8', '0', 'none', 'none', 1600207200, 1600293600, 1),
(786, 'KARTINI', '085813028961', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 56, 'no display', '0', 'none', 'none', 1600207200, 0, 1),
(787, 'ZAIN', '081342505635', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 57, 'cek , apa masalahnya', '0', 'none', 'none', 1600207200, 0, 1),
(788, 'IDC KOMPUTER', '085255988332', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 58, 'ganti keyboard', '0', 'none', 'none', 1600207200, 1600552800, 1),
(789, 'IDC KOMPUTER', '085255988332', 'Makassar', 7, 'none', 'none', 'PELANGGAN', 59, 'cek baterai', '0', 'none', 'none', 1600207200, 1605740400, 1),
(790, 'ZULKIFLI', '082292677765', 'Makassar', 7, 'none', 'none', 'PELANGGAN', 60, 'pesan baterai dan cek harga', '0', 'none', 'none', 1600207200, 1600552800, 1),
(791, 'BAKTI', '082192866008', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 61, 'ganti keyboard', '0', 'none', 'none', 1600207200, 1600293600, 1),
(792, 'FATUR', '081342188620', 'Makassar', 5, 'none', 'none', 'PELANGGAN', 62, 'cek layar? Garansi 9-9-2020', '0', 'none', 'none', 1600293600, 1600725600, 1),
(793, 'ATIKA', '085298043872', 'Makassar', 7, 'none', 'none', 'PELANGGAN', 63, 'pesan baterai', '0', 'none', 'none', 1600293600, 1600293600, 1),
(794, 'MEMET', '08114628234', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 64, 'cek apa yang bermasalah', '0', 'none', 'none', 1600293600, 1601935200, 1),
(795, 'SITI HAYATI', '085242929657', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 65, 'ganti keyboard', '0', 'none', 'none', 1600293600, 1600552800, 1),
(796, 'LEO', '085211010888', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 66, 'cek ? Matol', '0', 'none', 'none', 1600293600, 0, 1),
(797, 'SAMSIR INDAH CELL', '085298862682', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 67, 'cek ? Apa masalahnya', '0', 'none', 'none', 1600293600, 1605049200, 1),
(798, 'PAK JOHAN', '0811419252', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 68, 'matol', '0', 'none', 'none', 1600293600, 0, 1),
(799, 'ATO', '082188383248', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 69, 'none', '0', 'none', 'none', 1600380000, 1601676000, 1),
(800, 'KIARNO', '081239863043', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 70, 'tidak tampil kalau masuk windows engsel rusak', '0', 'none', 'none', 1600380000, 1600725600, 1),
(801, 'ANJAS', '085242901419', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 71, 'ganti keyboard dan install ulang', '0', 'none', 'none', 1600466400, 1600725600, 1),
(802, 'WIDHI APRINALDI', '085342774422', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 72, 'pesan keyboard, konfirmasi harga', '0', 'none', 'none', 1600466400, 0, 1),
(803, 'FITRI', '082321698495', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 73, 'ganti keyboard', '0', 'none', 'none', 1600552800, 1600552800, 1),
(804, 'DIDI', '082226414544', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 74, 'install windows 7', '0', 'none', 'none', 1600552800, 1600552800, 1),
(805, 'MARWAN', '081280591133', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 75, 'matol', '0', 'none', 'none', 1600552800, 1601071200, 1),
(806, 'JEREMI', '08114112046', 'Makassar', 5, 'none', 'none', 'TOKO / TEKNISI', 76, 'ganti lcd  ', '0', 'none', 'none', 1600552800, 1600725600, 1),
(807, 'ICCANG', '082293144456', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 77, 'servis engsel kiri, pasang keyboard sendiri', '0', 'none', 'none', 1600639200, 0, 1),
(808, 'ANDI EL MATRA', '081355267901', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 78, 'matol', '0', 'none', 'none', 1600639200, 1601330400, 1),
(809, 'RESA', '082348384070', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 79, 'install windows 10', '0', 'none', 'none', 1600639200, 0, 1),
(810, 'ABAS', '082291509346', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 80, 'pesan baterai asus a32-1015', '0', 'none', 'none', 1600725600, 1600725600, 1),
(811, 'AININ', '082293044741', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 81, 'power tidak tahan  ', '0', 'none', 'none', 1600812000, 1601589600, 1),
(812, 'UMA', '085299938539', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 82, 'ganti keyboard', '0', 'none', 'none', 1600898400, 1601330400, 1),
(813, 'HAJRUL', '082397001077', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 83, 'matol', '0', 'none', 'none', 1600898400, 0, 1),
(814, 'ADRI', '085299470258', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 84, 'cek, mati mati', '0', 'none', 'none', 1600898400, 1601330400, 1),
(815, 'SDI KATANGKA 1', '081354535580', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 85, 'install windows 10', '0', 'none', 'none', 1600898400, 0, 1),
(816, 'HARIONO', '081242864967', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 86, 'ganti keybaord dan install ulang', '0', 'none', 'none', 1600898400, 1601071200, 1),
(817, 'HARIONO', '081242864967', 'Makassar', 10, 'none', 'none', 'TOKO / TEKNISI', 87, 'cek panel usb lan', '0', 'none', 'none', 1600898400, 1601071200, 1),
(818, 'IRFAN', '081342366455', 'Makassar', 10, 'none', 'none', 'TOKO / TEKNISI', 88, 'tidak mau masuk windows', '0', 'none', 'none', 1600984800, 1601589600, 1),
(819, 'ZULKIFLI', '085256686566', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 89, 'no display', '0', 'none', 'none', 1601071200, 0, 1),
(820, 'IDIL', '081247244014', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 90, 'no display', '0', 'none', 'none', 1601071200, 1601589600, 1),
(821, 'NIAR', '085398275201', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 91, 'tidak mau masuk windows', '0', 'none', 'none', 1601157600, 1601589600, 1),
(822, 'JASULY', '085342070534', 'Makassar', 1, 'none', 'none', '', 92, 'no display', '0', 'none', 'none', 1601157600, 1602453600, 1),
(823, 'ANDI', '082320181994', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 93, 'ganti keybaord  ', '0', 'none', 'none', 1601244000, 1601330400, 1),
(824, 'ALIM', '082189322589', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 94, 'matol', '0', 'none', 'none', 1601244000, 1603317600, 1),
(825, 'LEGIO', '085299894398', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 95, 'install windows 7 dan ganti cmos', '0', 'none', 'none', 1601244000, 1601330400, 1),
(826, 'ANGGGI', '08656128387', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 96, 'install windows 10', '0', 'none', 'none', 1601244000, 1601330400, 1),
(827, 'AMIR', '081285768134', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 97, 'install windows 7', '0', 'none', 'none', 1601244000, 1601330400, 1),
(828, 'AMIR', '081285768134', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 98, 'cek wifi , tidak detek', '0', 'none', 'none', 1601244000, 1601330400, 1),
(829, 'HASANA KOMPUTER', '085230696002', 'Makassar', 4, 'none', 'none', 'TOKO / TEKNISI', 99, 'cek tidak mengisi baterai baru', '0', 'none', 'none', 1601416800, 1601762400, 1),
(830, 'ABDUL WAKIP', '081355217271', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 100, 'install windows 10', '0', 'none', 'none', 1601503200, 1601589600, 1),
(831, 'MUHAMMAD AMIN', '081355264777', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 101, 'tidak bisa menyala, cek tombol power', '0', 'none', 'none', 1601503200, 1601589600, 1),
(832, 'ILHAM', '081342714121', 'Makassar', 5, 'none', 'none', 'PELANGGAN', 102, 'ganti lcd , dan casing belakang lcd', '0', 'none', 'none', 1601503200, 1602799200, 1),
(833, 'ANGGA', '085255125577', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 103, 'install windows 7', '0', 'none', 'none', 1601589600, 1601589600, 1),
(834, 'ZULKIFLI', '085256686566', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 104, 'install windows 7', '0', 'none', 'none', 1601589600, 1601676000, 1),
(835, 'IBU MUNIRA', '085299832395', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 105, 'cek wifi   ', '0', 'none', 'none', 1601589600, 1601589600, 1),
(836, 'ZUL SMART KOMPUTER', '085259469298', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 106, 'matol', '0', 'none', 'none', 1601589600, 1605308400, 1),
(837, 'ANGEL', '082292995123', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 107, 'install windows 10, cek touchpad', '0', 'none', 'none', 1601589600, 1602021600, 1),
(838, 'LAILA', '085205622344', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 108, 'install spss 21', '0', 'none', 'none', 1601676000, 1601676000, 1),
(839, 'GIARNO', '081239863043', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 109, 'install windows 10, cek speaker (konfirmasi)', '0', 'none', 'none', 1601676000, 1602021600, 1),
(840, 'HABIBI', '085256253980', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 110, 'install windows 10', '0', 'none', 'none', 1601762400, 0, 1),
(841, 'SULAIMAN', '085342150287', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 111, 'matol', '0', 'none', 'none', 1601762400, 1603058400, 1),
(842, 'FAISAL', '081242958811', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 112, 'servis fleksibel', '0', 'none', 'none', 1601848800, 0, 1),
(843, 'HASYIM', '08111019802', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 113, 'matol', '0', 'none', 'none', 1601935200, 1605567600, 1),
(844, 'POSEL', '082193050409', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 114, 'no display', '0', 'none', 'none', 1601935200, 1603317600, 1),
(845, 'IRFAN', '081342366455', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 115, 'matol', '0', 'none', 'none', 1601935200, 1607382000, 1),
(846, 'MEMET', '08114628234', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 116, 'install windows 7', '0', 'none', 'none', 1602021600, 1603576800, 1),
(847, 'MEMET', '08114628234', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 117, 'matol', '0', 'none', 'none', 1602021600, 0, 1),
(848, 'DARWAN', '0811414014', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 118, 'cek, speaker tidak bunyi', '0', 'none', 'none', 1602021600, 1604876400, 1),
(849, 'YUYU', '085242590253', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 119, 'no display', '0', 'none', 'none', 1602021600, 0, 1),
(850, 'RAHMAT T', '08114442633', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 120, 'service engsel kanan, cek audio juga', '0', 'none', 'none', 1602108000, 1602799200, 1),
(851, '72 KOMPUTER', '085340555567', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 121, 'cek', '0', 'none', 'none', 1602108000, 1602799200, 1),
(852, 'ZULKIFLI', '085256686566', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 122, 'no display', '0', 'none', 'none', 1602108000, 1603666800, 1),
(853, 'UCA', '085340552559', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 123, 'no display', '0', 'none', 'none', 1602108000, 1604876400, 1),
(854, 'RATNA', '081288810581', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 124, 'garansi mesin 26 sep 2020', '0', 'none', 'none', 1602453600, 1604876400, 1),
(855, 'RATNA', '082196881662', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 125, 'install windows 7', '0', 'none', 'none', 1602453600, 1602799200, 1),
(856, 'IKA', '082290887121', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 126, 'install windows 10, ganti hdd', '0', 'none', 'none', 1602540000, 1602626400, 1),
(857, 'GERALDI', '082345265313', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 127, 'install mac os 250000', '0', 'none', 'none', 1602540000, 1602626400, 1),
(858, 'IPUL', '085394053561', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 128, 'matol', '0', 'none', 'none', 1602626400, 1603576800, 1),
(859, 'AMRAN', '081342541006', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 129, 'matol', '0', 'none', 'none', 1602626400, 0, 1),
(860, 'RUDI', '082333336730', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 130, 'cek tombol power, dan mati-mati', '0', 'none', 'none', 1602712800, 1603317600, 1),
(861, 'RUDI', '082333336730', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 131, 'cek keseluruhan, lambat', '0', 'none', 'none', 1602712800, 0, 1),
(862, 'APPI', '085399134362', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 132, 'cek kipas, install windows 8 juga', '0', 'none', 'none', 1602799200, 0, 1),
(863, 'DIDI', '082293090013', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 133, 'install windows 10', '0', 'none', 'none', 1602799200, 1603666800, 1),
(864, 'DIDI', '082293090013', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 134, 'matol', '0', 'none', 'none', 1602799200, 1603666800, 1),
(865, 'IKRAM', '085242086995', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 135, 'install windows 7', '0', 'none', 'none', 1602799200, 1602799200, 1),
(866, 'REZKI', '085284423076', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 136, 'matol', '0', 'none', 'none', 1602799200, 0, 1),
(867, 'ADI', '085717777928', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 137, 'no display', '0', 'none', 'none', 1602972000, 0, 1),
(868, 'RIDWAN', '08111161135', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 138, 'kalau di pake 4 jam mati, garansi 20 - 09 - 2020', '0', 'none', 'none', 1602972000, 1603058400, 1),
(869, 'REZKI', '085284423076', 'Makassar', 10, 'none', 'none', 'TOKO / TEKNISI', 139, 'panjar 300000', '0', 'none', 'none', 1602972000, 1603058400, 1),
(870, 'AMIR', '081285768134', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 140, 'install windows 7', '0', 'none', 'none', 1603058400, 1603144800, 1),
(871, 'UCA', '085340552559', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 141, 'matol', '0', 'none', 'none', 1603058400, 1604876400, 1),
(872, 'UCA', '085340552559', 'Makassar', 10, 'none', 'none', 'TOKO / TEKNISI', 142, 'cek apa yang bermasalah', '0', 'none', 'none', 1603058400, 1604876400, 1),
(873, 'SYAHRUL', '085343751157', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 143, 'cek , kalau goyang mati', '0', 'none', 'none', 1603144800, 0, 1),
(874, 'ANAS', '081355838338', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 144, 'cek, kadang power kadang tidak', '0', 'none', 'none', 1603144800, 1603666800, 1),
(875, 'ERIC', '08114112646', 'Makassar', 8, 'none', 'none', 'PELANGGAN', 145, 'pasang SSD , RAM 8GB, caddy', '0', 'none', 'none', 1603231200, 1603576800, 1),
(876, 'IKA', '082290887121', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 146, 'Cek ? sudah ganti HDD , speaker tidak bunyi', '0', 'none', 'none', 1603231200, 1603317600, 1),
(877, 'NUR HAYATI', '085346105654', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 147, 'matol, kalau sudah menayla gantai baterai juga', '0', 'none', 'none', 1603317600, 0, 1),
(878, 'FAHRI', '082191790552', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 148, 'no display', '0', 'none', 'none', 1603317600, 0, 1),
(879, 'ANDI MUHAMMAD ISMAIL', '082346990060', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 149, 'no display', '0', 'none', 'none', 1603404000, 0, 1),
(880, 'MEMET', '08114628234', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 150, 'matol', '0', 'none', 'none', 1603404000, 0, 1),
(881, 'ANAS', '081355838338', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 151, 'ganti keybaord', '0', 'none', 'none', 1603404000, 0, 1),
(882, 'HILDAYANTI', '082379409996', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 152, 'install windows 10', '0', 'none', 'none', 1603404000, 1603576800, 1),
(883, 'ARIS', '085299693680', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 153, 'install windows 10', '0', 'none', 'none', 1603404000, 1603490400, 1),
(884, 'UTTA', '085299903901', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 154, 'no display', '0', 'none', 'none', 1603404000, 1604876400, 1),
(885, 'FADIL', '081342046103', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 155, 'ganti keyboard', '0', 'none', 'none', 1603490400, 1603753200, 1),
(886, 'HAIDIL', '085261223205', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 156, 'ganti keyboard dan install windows 10', '0', 'none', 'none', 1603576800, 1603666800, 1),
(887, 'SAKWAN', '0895800907342', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 157, 'install windows 10', '0', 'none', 'none', 1603576800, 1603666800, 1),
(888, 'AINUL', '081350013262', 'Makassar', 6, 'none', 'none', 'TOKO / TEKNISI', 158, 'install windows 10', '0', 'none', 'none', 1603576800, 0, 1),
(889, 'SAIN', '081342505635', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 159, 'matol', '0', 'none', 'none', 1603666800, 0, 1),
(890, 'FAUZAN', '082393357893', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 160, 'matol', '0', 'none', 'none', 1603666800, 1604185200, 1),
(891, 'FAUZAN', '082393357893', 'Makassar', 4, 'none', 'none', 'TOKO / TEKNISI', 161, 'ganti keyboard', '0', 'none', 'none', 1603666800, 1604185200, 1),
(892, 'FATUR', '081342188620', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 162, 'cek kamera', '0', 'none', 'none', 1603753200, 0, 1),
(893, 'HERI', '081247878078', 'Makassar', 10, 'none', 'none', '', 163, 'servis engsel', '0', 'none', 'none', 1603753200, 0, 1),
(894, 'JUNAIDI', '085255023454', 'Makassar', 10, 'none', 'none', 'TOKO / TEKNISI', 164, 'cek masalah', '0', 'none', 'none', 1603753200, 0, 1),
(895, 'FITRA PENGADILAN', '085255005069', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 165, 'no display', '0', 'none', 'none', 1603839600, 1605654000, 1),
(896, 'FIKAR', '085855899801', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 166, 'matol', '0', 'none', 'none', 1603839600, 0, 1),
(897, 'CIA', '082346532197', 'Makassar', 10, 'none', 'none', '', 167, 'cek, apa masalahnya', '0', 'none', 'none', 1603839600, 0, 1),
(898, 'CATABA KOMPUTER', '082345269507', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 168, 'mati-mati', '0', 'none', 'none', 1604012400, 1604876400, 1),
(899, 'MAULANA', '085238869221', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 169, 'install windows 8', '0', 'none', 'none', 1604012400, 1604185200, 1),
(900, 'NURHADI', '089516905494', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 170, 'ganti keyboard', '0', 'none', 'none', 1604012400, 1604185200, 1),
(901, 'HASANAH KOMPUTER', '085230696002', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 171, 'matol', '0', 'none', 'none', 1604185200, 0, 1),
(902, 'TIAR', '085239944729', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 172, 'install windows 10', '0', 'none', 'none', 1604185200, 1604358000, 1),
(903, 'ALTERGA', '085255005069', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 173, 'none', '0', 'none', 'none', 1604185200, 0, 1),
(904, 'SULTAN', '082396769115', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 174, 'install windows 10', '0', 'none', 'none', 1604271600, 1604358000, 1),
(905, 'NURUL', '082399309790', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 175, 'install windows 10', '0', 'none', 'none', 1604358000, 1604358000, 1),
(906, 'MUHAJIRIN', '085343625265', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 176, 'cek jack power', '0', 'none', 'none', 1604358000, 0, 1),
(907, 'RATNA', '081288810581', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 177, 'garansi mesin 26 sep 2020', '0', 'none', 'none', 1604444400, 1605135600, 1),
(908, 'ARFIANI', '085230970706', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 178, 'cek kipas bunyi-bunyi', '0', 'none', 'none', 1604444400, 1604876400, 1),
(909, 'DIRGA', '082346394037', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 179, 'bersihkan, kondisi mati-mati', '0', 'none', 'none', 1604444400, 1607036400, 1),
(910, 'UDIN', '081243619978', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 180, 'cek, lambat', '0', 'none', 'none', 1604444400, 1604876400, 1),
(911, 'NUR FADILLAH KOMPUTER', '081342520100', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 181, 'matol', '0', 'none', 'none', 1604530800, 1606086000, 1),
(912, 'NUR FADILLAH KOMPUTER', '081342520100', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 182, 'no display', '0', 'none', 'none', 1604530800, 1606086000, 1),
(913, 'MAUMERE KOMPUTER', '081354939098', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 183, 'no display', '0', 'none', 'none', 1604530800, 0, 1),
(914, 'FAISAL', '082337038444', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 184, 'cek , mic tidak bersuara', '0', 'none', 'none', 1604530800, 1604703600, 1),
(915, 'SYAMSUL', '085255310060', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 185, 'matol', '0', 'none', 'none', 1604530800, 0, 1),
(916, 'SYAMSUL', '085255310060', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 186, 'cek, kadang power kadang tidak , dan klau power masuknya lambat', '0', 'none', 'none', 1604530800, 0, 1),
(917, 'AAN', '085314137484', 'Makassar', 1, 'none', 'none', '', 187, 'matol', '0', 'none', 'none', 1604530800, 0, 1),
(918, 'ARWIN', '085343965917', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 188, 'cek, apa masalahnya', '0', 'none', 'none', 1604790000, 0, 1),
(919, 'RUSLAN', '085242259010', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 189, 'install windows 7', '0', 'none', 'none', 1604876400, 1605222000, 1),
(920, 'LEO', '085211010888', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 190, 'matol', '0', 'none', 'none', 1604876400, 1605049200, 1),
(921, 'RIFKA ANISA', '085340638598', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 191, 'install windows 10', '0', 'none', 'none', 1604876400, 0, 1),
(922, 'BOBI', '082195161789', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 192, 'install windows 10', '0', 'none', 'none', 1604876400, 0, 1),
(923, 'ANA', '082393086634', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 193, 'install windows 10', '0', 'none', 'none', 1604876400, 0, 1),
(924, 'SUBHAN', '082152885255', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 194, 'install windows 7', '0', 'none', 'none', 1604876400, 1605135600, 1),
(925, 'ASRUL', '08135522668', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 195, 'ganti ssd 500Gb', '0', 'none', 'none', 1604962800, 0, 1),
(926, 'IPUL', '085255026706', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 196, 'install windows 10', '0', 'none', 'none', 1604962800, 0, 1),
(927, 'FAUZAN', '082393357893', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 197, 'no display', '0', 'none', 'none', 1604962800, 1607641200, 1),
(928, 'FAUZAN', '082393357893', 'Makassar', 10, 'none', 'none', 'TOKO / TEKNISI', 198, 'cek masalah', '0', 'none', 'none', 1604962800, 0, 1),
(929, 'GAFFAR', '085255509679', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 199, 'service engsel  ', '0', 'none', 'none', 1604962800, 1605394800, 1),
(930, 'FIRA', '082346839421', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 200, 'cek, tidak mau power', '0', 'none', 'none', 1605049200, 1605135600, 1),
(931, 'ULFA', '085342337659', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 201, 'pesan keyboard', '0', 'none', 'none', 1605049200, 1605222000, 1),
(932, 'MARSUDIN', '081935240638', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 202, 'install windows 10', '0', 'none', 'none', 1605135600, 1605135600, 1),
(933, 'ICCANG', '082293144456', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 203, 'ganti keybaord dan install windows 7', '0', 'none', 'none', 1605135600, 1605135600, 1),
(934, 'SAIPUL', '085242681979', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 204, 'cek , tidak masuk windows', '0', 'none', 'none', 1605135600, 0, 1),
(935, 'AMAR', '082193252882', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 205, 'cek, tampil gelap', '0', 'none', 'none', 1605135600, 0, 1),
(936, 'RAIHAN', '085398330526', 'Makassar', 5, 'none', 'none', 'PELANGGAN', 206, 'ganti layar', '0', 'none', 'none', 1605135600, 1605222000, 1),
(937, 'AWALUDDIN', '085343608053', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 207, 'no display', '0', 'none', 'none', 1605135600, 1605654000, 1),
(938, 'IKRAM', '085242086995', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 208, 'install windows 7', '0', 'none', 'none', 1605222000, 1605222000, 1),
(939, 'FATUR', '082393050717', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 209, 'cek, apa masalahnya', '0', 'none', 'none', 1605222000, 1606863600, 1),
(940, 'NUR AISYA', '085242582512', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 210, 'ganti keyboard', '0', 'none', 'none', 1605222000, 1605394800, 1),
(941, 'SAMSUSDIN', '085242028403', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 211, 'matol', '0', 'none', 'none', 1605222000, 0, 1),
(942, 'APRISAL', '082345678957', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 212, 'mati-mati', '0', 'none', 'none', 1605222000, 1605913200, 1),
(943, 'RATNA', '082196881662', 'Makassar', 10, 'none', 'none', '', 213, 'cek, masalah di layar', '0', 'none', 'none', 1605222000, 1606086000, 1),
(944, 'SAIFUL', '085242681979', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 214, 'pasang hdd sendiri, dan install windows 10', '0', 'none', 'none', 1605308400, 1605481200, 1),
(945, 'RUDI', '082333336730', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 215, 'ganti keyboard', '0', 'none', 'none', 1605394800, 0, 1),
(946, 'AMIR', '085215321673', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 216, 'cek, harga baterai', '0', 'none', 'none', 1605394800, 1606086000, 1),
(947, 'DAHLAN', '081341964113', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 217, 'install windows 10', '0', 'none', 'none', 1605481200, 1606086000, 1),
(948, 'HAEDAR', '085146027922', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 218, 'install windows 10', '0', 'none', 'none', 1605481200, 0, 1),
(949, 'EKAHARUDDIN', '081242989310', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 219, 'matol', '0', 'none', 'none', 1605481200, 1606345200, 1),
(950, 'AMIR', '085215321673', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 220, 'pesan baterai, panjar 200.000', '0', 'none', 'none', 1605481200, 0, 1),
(951, 'HABIBI', '081355756485', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 221, 'matol', '0', 'none', 'none', 1605481200, 1606086000, 1),
(952, 'FADIL', '081340069004', 'Makassar', 5, 'none', 'none', 'PELANGGAN', 222, 'ganti lcd', '0', 'none', 'none', 1605481200, 1605654000, 1),
(953, 'JUNAIDI', '085255023454', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 223, 'no display', '0', 'none', 'none', 1605481200, 0, 1),
(954, 'RAFLI', '081319233861', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 224, 'cek, layar hitam', '0', 'none', 'none', 1605481200, 1606086000, 1),
(955, 'MAHDALENA', '085240222347', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 225, 'ganti keyboard, garansi 26-08-2020 masuk kembali 18-11-2020', '0', 'none', 'none', 1605654000, 1605654000, 1),
(956, 'IJAH', '085238723143', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 226, 'cek, tidak mau masuk windows', '0', 'none', 'none', 1605654000, 1607641200, 1),
(957, 'ANCA', '085242590253', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 227, 'cek, konfirmasi dahulu kalau masalah mesin, kalau bukan langsung kerja engsel kiri dan kanan', '0', 'none', 'none', 1605654000, 0, 1),
(958, 'INDRA', '082229416584', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 228, 'suara tidak bunyi', '0', 'none', 'none', 1605654000, 1606172400, 1),
(959, 'ARDI', '085244801806', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 229, 'ganti keyboard', '0', 'none', 'none', 1605654000, 1606086000, 1),
(960, 'VERA', '085242168891', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 230, 'install windows 7 dan ganti cmos', '0', 'none', 'none', 1605740400, 1606863600, 1),
(961, 'DERI', '081243369309', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 231, 'matol', '0', 'none', 'none', 1605740400, 1606345200, 1),
(962, 'DERI', '081243369309', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 232, 'power tidak tahan kalau pasang memori', '0', 'none', 'none', 1605740400, 0, 1),
(963, 'TAKDIR', '085242088619', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 233, 'tombol power rusak, cek harga casing juga', '0', 'none', 'none', 1605740400, 0, 1),
(964, 'RATNA', '081288810581', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 234, 'matol', '0', 'none', 'none', 1605740400, 1607382000, 1),
(965, 'HASANA KOMPUTER', '085230696002', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 235, 'matol / garansi 10 no 2020', '0', 'none', 'none', 1605740400, 0, 1),
(966, 'HASANAH KOMPUTER', '085230696002', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 236, 'no display', '0', 'none', 'none', 1605740400, 0, 1),
(967, 'ENAL BONE', '081355301302', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 237, 'cek, apa masalahnya', '0', 'none', 'none', 1605826800, 0, 1),
(968, 'ENAL BONE', '081355301302', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 238, 'install windows 7', '0', 'none', 'none', 1605826800, 0, 1),
(969, 'ENAL BONE', '081355301302', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 239, 'mati-mati', '0', 'none', 'none', 1605826800, 0, 1),
(970, 'AHMAD JAIS', '082344979443', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 240, 'matol', '0', 'none', 'none', 1605826800, 0, 1),
(971, 'CIA', '082346532197', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 241, 'ganti keyboard/ garansi 13/11/2020', '0', 'none', 'none', 1605826800, 1606086000, 1),
(972, 'RAHMI', '081245486733', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 242, 'install windows 10', '0', 'none', 'none', 1605913200, 1606086000, 1),
(973, 'AKIP', '081355343638', 'Makassar', 6, 'none', 'none', '', 243, 'install windows 10', '0', 'none', 'none', 1605913200, 1606086000, 1),
(974, 'IMMA', '082290033334', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 244, 'install windows 10', '0', 'none', 'none', 1606086000, 0, 1),
(975, 'FITRI', '081241422898', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 245, 'install windows 7', '0', 'none', 'none', 1606086000, 0, 1),
(976, 'NUR FADILLAH KOMPUTER', '081342520100', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 246, 'no display', '0', 'none', 'none', 1606086000, 0, 1),
(977, 'JABAL LAPAK HIJAU', '081340484248', 'Makassar', 0, 'none', 'none', 'TOKO / TEKNISI', 247, '', '0', 'none', 'none', 1606172400, 0, 1),
(978, 'HAIRUL', '0v81248722509', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 248, 'no display', '0', 'none', 'none', 1606172400, 0, 1),
(979, 'RIAN', '082345376430', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 249, 'matol', '0', 'none', 'none', 1606258800, 0, 1),
(980, 'ZULKIFLI', '085256686566', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 250, 'cek, suara tidak mau keluar', '0', 'none', 'none', 1606258800, 1607382000, 1),
(981, 'HASNUR', '085343629190', 'Makassar', 5, 'none', 'none', 'PELANGGAN', 251, 'ganti layar', '0', 'none', 'none', 1606258800, 0, 1),
(982, 'IFA', '082291512886', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 252, 'install windows 10', '0', 'none', 'none', 1606345200, 0, 1),
(983, 'wira', '081355535504', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 253, 'cek, lambat', '0', 'none', 'none', 1606345200, 0, 1),
(984, 'BAKTIAR', '082399446867', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 254, 'buat fleksibel', '0', 'none', 'none', 1606431600, 0, 1),
(985, 'NUR', '082150449741', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 255, 'no display', '0', 'none', 'none', 1606431600, 0, 1),
(986, 'UDI', '081242307788', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 256, 'hdd saja', '0', 'none', 'none', 1606431600, 1606518000, 1),
(987, 'AKIB', '081355343638', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 257, 'cek, ? Sering hang + kalau d shutdown lampu power tidak mati', '0', 'none', 'none', 1606518000, 1606777200, 1),
(988, 'FAUZAN', '082393357893', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 258, 'no display', '0', 'none', 'none', 1606518000, 0, 1),
(989, 'SCF', '082349904171', 'Makassar', 8, 'none', 'none', 'PELANGGAN', 259, 'ganti hdd ssd', '0', 'none', 'none', 1606518000, 0, 1),
(990, 'DANI', '085242752626', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 260, 'cek, adaptor di cabut langsung mati', '0', 'none', 'none', 1606604400, 0, 1),
(991, 'ENI', '082346066061', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 261, 'matol', '0', 'none', 'none', 1606604400, 1606777200, 1),
(992, 'IKRAM', '085242086995', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 262, 'install windows 7', '0', 'none', 'none', 1606690800, 0, 1),
(993, 'ALI', '085342146766', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 263, 'isntall windows 7', '0', 'none', 'none', 1606690800, 0, 1),
(994, 'IWAN', '082111025666', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 264, 'cek, tombol kiri kanan dan touchpad tidak berfungsi', '0', 'none', 'none', 1606777200, 0, 1),
(995, 'YAHDI', '085395118389', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 265, 'ganti keyboard dan cek speaker', '0', 'none', 'none', 1606777200, 0, 1),
(996, 'FAHRI', '082187653434', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 266, 'install windows 10', '0', 'none', 'none', 1606950000, 0, 1),
(997, 'BUR', '081241887303', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 267, 'cek, kameranya', '0', 'none', 'none', 1607036400, 0, 1),
(998, 'SARWONO', '081222228483', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 268, 'matol', '0', 'none', 'none', 1607036400, 0, 1),
(999, 'ERLINA', '081244123442', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 269, 'ganti keyboard dan pesan adaptor', '0', 'none', 'none', 1607209200, 1607382000, 1),
(1000, 'TKN INSAN CEMERLANG', '081242944444', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 270, 'install windows 7', '0', 'none', 'none', 1607209200, 0, 1),
(1001, 'SAWIR', '081342770327', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 271, 'ganti keyboard', '0', 'none', 'none', 1607295600, 1607295600, 1),
(1002, 'SAWIR', '081342770327', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 272, 'install windows 7', '0', 'none', 'none', 1607295600, 1607295600, 1),
(1003, 'REZKI', '085284423076', 'Makassar', 10, 'none', 'none', 'TOKO / TEKNISI', 273, 'ganti cmos', '0', 'none', 'none', 1607295600, 1607986800, 1),
(1004, 'AJIR', '085340968573', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 274, 'no display', '0', 'none', 'none', 1607382000, 1608159600, 1),
(1005, 'AIDAR', '085146027922', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 275, 'install windows 10', '0', 'none', 'none', 1607382000, 1607641200, 1),
(1006, 'UMMU', '082293709416', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 276, 'install office 2013 , dan cek wifi tidak detek', '0', 'none', 'none', 1607382000, 1607382000, 1),
(1007, 'ZAKI', '085255239534', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 277, 'mati-mati', '0', 'none', 'none', 1607382000, 0, 1),
(1008, 'ZAKI', '085255239534', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 278, 'mati-mati', '0', 'none', 'none', 1607382000, 0, 1),
(1009, 'ANSAR', '081342333828', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 279, 'install windows 7', '0', 'none', 'none', 1607382000, 1607641200, 1),
(1010, 'MUFLI', '085269380719', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 280, 'matol', '0', 'none', 'none', 1607554800, 0, 1),
(1011, 'NURUL', '085256836332', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 281, 'ganti keyboard', '0', 'none', 'none', 1607554800, 1607641200, 1),
(1012, 'SAFAR', '085341140220', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 282, 'no display', '0', 'none', 'none', 1607554800, 1607727600, 1),
(1013, 'TIARA', '082349687656', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 283, 'power tidak tahan', '0', 'none', 'none', 1607641200, 0, 1),
(1014, 'FITRI', '081241422898', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 284, 'install windows 7', '0', 'none', 'none', 1607641200, 1607900400, 1),
(1015, 'FITRI', '081241422898', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 285, 'cek hdd', '0', 'none', 'none', 1607641200, 1607900400, 1),
(1016, 'EDI', '082189552345', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 286, 'tidak mau power', '0', 'none', 'none', 1607641200, 0, 1),
(1017, 'CATABA KOMPUTER', '082345269507', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 287, 'mati-mati / garansi 6-11-2020 ( masalahnya sama)', '0', 'none', 'none', 1607641200, 0, 1),
(1018, 'FIRMAN', '085346999928', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 288, 'install windows 10', '0', 'none', 'none', 1607814000, 1607900400, 1),
(1019, 'NAWIR', '085255431992', 'Makassar', 2, 'none', 'none', 'PELANGGAN', 289, 'service engsel kiri', '0', 'none', 'none', 1607900400, 0, 1),
(1020, 'DAHLAN', '081341964113', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 290, 'install windows 10', '0', 'none', 'none', 1607986800, 1607986800, 1),
(1021, 'ISLAM', '082187641984', 'Makassar', 5, 'none', 'none', 'PELANGGAN', 291, 'ganti layar', '0', 'none', 'none', 1607986800, 1608159600, 1),
(1022, 'MULYADI', '082195571986', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 292, 'matol', '0', 'none', 'none', 1607986800, 0, 1),
(1023, 'MULYADI', '082195571986', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 293, 'cek, hanya sampai di logo', '0', 'none', 'none', 1607986800, 0, 1),
(1024, 'MULYADI', '082195571986', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 294, 'cek, hang di logo', '0', 'none', 'none', 1607986800, 0, 1),
(1025, 'RAHMAT PENGADILAN AGAMA', '08114442633', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 295, 'cek keseluruhan  ', '0', 'none', 'none', 1607986800, 0, 1),
(1026, 'ZUL MART KOMPUTER', '085259469298', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 296, 'matol', '0', 'none', 'none', 1608073200, 0, 1),
(1027, 'MARHABAN', '081355054132', 'Makassar', 6, 'none', 'none', 'PELANGGAN', 297, 'install windows 10', '0', 'none', 'none', 1608073200, 1608159600, 1),
(1028, 'HIDAYAT HALIN', '085399838133', 'Makassar', 0, 'none', 'none', '', 298, '', '0', 'none', 'none', 1608159600, 0, 1),
(1029, 'RIAN', '085267897008', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 299, 'ganti keyboard', '0', 'none', 'none', 1608159600, 0, 1),
(1030, 'HAERUDDIN', '082296624321', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 300, 'no display', '0', 'none', 'none', 1608591600, 0, 1),
(1031, 'HAERUDDIN', '082296624321', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 301, 'matol', '0', 'none', 'none', 1608591600, 0, 1),
(1032, 'HAERUDDIN', '082296624321', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 302, 'matol', '0', 'none', 'none', 1608591600, 0, 1),
(1033, 'HAERUDDIN', '082296624321', 'Makassar', 1, 'none', 'none', 'TOKO / TEKNISI', 303, 'matol', '0', 'none', 'none', 1608591600, 0, 1),
(1034, 'AZISAH', '082187335836', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 304, 'pesan adaptor', '0', 'none', 'none', 1608591600, 1609023600, 1),
(1035, 'LULU', '082293366756', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 305, 'no display / mesin ok ganti keyboardnya', '0', 'none', 'none', 1608591600, 1609282800, 1),
(1036, 'ICCANG', '0822293144456', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 306, 'servis klik kiri', '0', 'none', 'none', 1608591600, 1609023600, 1),
(1037, 'SAMSIR INDAH CELL', '085298862682', 'Makassar', 2, 'none', 'none', 'TOKO / TEKNISI', 307, 'engsel kanan / datang terbongkar', '0', 'none', 'none', 1608591600, 0, 1),
(1038, 'ASRI', '085396441195', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 308, 'matol', '0', 'none', 'none', 1608678000, 1609023600, 1),
(1039, 'TRI', '085397378722', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 309, 'cek wifi, install driver dulu', '0', 'none', 'none', 1608678000, 1608678000, 1),
(1040, 'ACO', '082189812262', 'Makassar', 3, 'none', 'none', 'PELANGGAN', 310, 'servis engsel kiri kanan', '0', 'none', 'none', 1608678000, 0, 1),
(1041, 'MILDA', '085399532891', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 311, 'matol', '0', 'none', 'none', 1608937200, 0, 1),
(1042, 'PINA', '085396403818', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 312, 'matol', '0', 'none', 'none', 1608937200, 1609282800, 1),
(1043, 'AAD', '082196287600', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 313, 'ganti speaker', '0', 'none', 'none', 1608937200, 0, 1),
(1044, 'SURAHMAN', '085255009899', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 314, 'tampil gelap / kalau bagus install windows 7', '0', 'none', 'none', 1609023600, 1609023600, 1),
(1045, 'AKBAR', '082349725385', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 315, 'baut tidak mau terbuka', '0', 'none', 'none', 1609023600, 1609023600, 1),
(1046, 'MUKTASIM', '092193047805', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 316, 'matol', '0', 'none', 'none', 1609023600, 1609110000, 1),
(1047, 'GANIA', '081342318485', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 317, 'ganti keyboard', '0', 'none', 'none', 1609110000, 0, 1),
(1048, 'HATTA', '0811412172', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 318, 'no display', '0', 'none', 'none', 1609110000, 0, 1),
(1049, 'MUSTAZAM', '082344911388', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 319, 'kadang tampil kadang  tidak', '0', 'none', 'none', 1609110000, 0, 1),
(1050, 'FAJAR', '082348438216', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 320, 'matol', '0', 'none', 'none', 1609282800, 0, 1),
(1051, 'LUKMAN', '085399490937', 'Makassar', 5, 'none', 'none', 'PELANGGAN', 321, 'ganti layar', '0', 'none', 'none', 1609282800, 0, 1),
(1052, 'BUR', '081241887363', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 322, 'cek, lambat', '0', 'none', 'none', 1609282800, 0, 1),
(1053, 'WIN', '082188332021', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 323, 'matol', '0', 'none', 'none', 1609369200, 0, 1),
(1054, 'ANSAR', '085242197723', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 324, 'no display', '0', 'none', 'none', 1609369200, 0, 1),
(1055, 'ANSAR', '085242197723', 'Makassar', 1, 'none', 'none', 'PELANGGAN', 325, 'no display', '0', 'none', 'none', 1609369200, 0, 1),
(1056, 'ASHAR', '082347682578', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 326, 'baterai tidak mengisi', '0', 'none', 'none', 1609369200, 0, 1),
(1057, 'AKIB', '081355343638', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 327, 'cek, sering hang', '0', 'none', 'none', 1609369200, 0, 1),
(1058, 'YORI', '085242156736', 'Makassar', 4, 'none', 'none', 'PELANGGAN', 328, 'cek keyboard', '0', 'none', 'none', 1609542000, 0, 1),
(1059, 'INDAH', '085796895049', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 329, 'cek, kalau nyala langsung ke bios', '0', 'none', 'none', 1609714800, 0, 1),
(1060, 'SYAMSYAHRIL', '088242868611', 'Makassar', 10, 'none', 'none', 'PELANGGAN', 330, 'ganti layar sama cek wifi', '0', 'none', 'none', 1609714800, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `costumer_jenis_perbaikan`
--

CREATE TABLE `costumer_jenis_perbaikan` (
  `jenis_perbaikan_id` int(11) NOT NULL,
  `jenis_perbaikan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `costumer_jenis_perbaikan`
--

INSERT INTO `costumer_jenis_perbaikan` (`jenis_perbaikan_id`, `jenis_perbaikan`) VALUES
(1, 'SERVICE MOTHERBOARD/MESIN'),
(2, 'SERVICE 1 ENGSEL'),
(3, 'SERVICE 2 ENGSEL'),
(4, 'GANTI KEYBOARD'),
(5, 'GANTI LAYAR/LCD'),
(6, 'INSTALL OS'),
(7, 'GANTI BATERAI'),
(8, 'GANTI HARDDISK'),
(10, 'LAIN-LAIN\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `costumer_laptop`
--

CREATE TABLE `costumer_laptop` (
  `laptop_id` int(11) NOT NULL,
  `ukuran_id` int(11) NOT NULL,
  `merk_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `warna_id` int(11) NOT NULL,
  `kondisi_fisik_id` int(11) NOT NULL,
  `kelengkapan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `costumer_laptop`
--

INSERT INTO `costumer_laptop` (`laptop_id`, `ukuran_id`, `merk_id`, `model_id`, `warna_id`, `kondisi_fisik_id`, `kelengkapan_id`) VALUES
(1, 5, 3, 9, 1, 1, 1),
(2, 5, 2, 10, 1, 1, 1),
(3, 1, 1, 11, 1, 1, 1),
(4, 0, 7, 12, 1, 1, 1),
(5, 5, 5, 13, 1, 1, 1),
(6, 7, 7, 14, 1, 1, 1),
(7, 5, 5, 13, 1, 1, 1),
(8, 2, 1, 15, 1, 1, 1),
(9, 1, 1, 16, 1, 1, 1),
(10, 5, 1, 11, 1, 1, 1),
(11, 5, 2, 17, 1, 1, 1),
(12, 2, 3, 18, 1, 1, 1),
(13, 6, 5, 19, 1, 1, 1),
(14, 5, 5, 20, 1, 1, 1),
(15, 2, 3, 18, 1, 1, 1),
(16, 9, 1, 21, 1, 1, 1),
(17, 6, 3, 22, 1, 1, 1),
(18, 5, 5, 23, 1, 1, 1),
(19, 5, 8, 24, 1, 1, 1),
(20, 1, 2, 25, 1, 1, 1),
(21, 5, 5, 26, 1, 1, 1),
(22, 5, 2, 27, 1, 1, 1),
(23, 2, 1, 28, 1, 1, 1),
(24, 5, 2, 17, 1, 1, 1),
(25, 5, 1, 21, 1, 1, 1),
(26, 1, 1, 29, 1, 1, 1),
(27, 0, 9, 30, 1, 1, 1),
(28, 1, 1, 25, 1, 1, 1),
(29, 5, 5, 31, 1, 1, 1),
(30, 5, 3, 32, 1, 1, 1),
(31, 5, 5, 33, 1, 1, 1),
(32, 6, 5, 19, 1, 1, 1),
(33, 10, 0, 0, 1, 1, 1),
(34, 2, 3, 4, 1, 1, 1),
(35, 5, 1, 8, 1, 1, 1),
(36, 5, 2, 34, 1, 1, 1),
(37, 5, 2, 34, 1, 1, 1),
(38, 2, 3, 4, 1, 1, 1),
(39, 5, 1, 35, 1, 1, 1),
(40, 2, 3, 36, 1, 1, 1),
(41, 5, 1, 1, 1, 1, 1),
(42, 5, 1, 37, 1, 1, 1),
(43, 8, 1, 38, 1, 1, 1),
(44, 5, 2, 39, 1, 1, 1),
(45, 2, 1, 40, 1, 1, 1),
(46, 2, 8, 41, 1, 1, 1),
(47, 5, 3, 32, 1, 1, 1),
(48, 5, 1, 42, 1, 1, 1),
(49, 5, 10, 43, 1, 1, 1),
(50, 2, 3, 18, 1, 1, 1),
(51, 5, 1, 44, 1, 1, 1),
(52, 5, 3, 45, 1, 1, 1),
(53, 1, 8, 46, 1, 1, 1),
(54, 5, 8, 47, 1, 1, 1),
(55, 5, 1, 48, 1, 1, 1),
(56, 2, 1, 49, 1, 1, 1),
(57, 5, 1, 50, 1, 1, 1),
(58, 5, 3, 51, 1, 1, 1),
(59, 2, 1, 52, 1, 1, 1),
(60, 0, 7, 0, 1, 1, 1),
(61, 5, 5, 20, 1, 1, 1),
(62, 5, 5, 26, 1, 1, 1),
(63, 5, 3, 51, 1, 1, 1),
(64, 5, 1, 8, 1, 1, 1),
(65, 5, 3, 45, 1, 1, 1),
(66, 2, 1, 53, 1, 1, 1),
(67, 2, 3, 4, 1, 1, 1),
(68, 7, 0, 0, 1, 1, 1),
(69, 5, 8, 54, 1, 1, 1),
(70, 6, 8, 55, 1, 1, 1),
(71, 2, 1, 56, 1, 1, 1),
(72, 5, 8, 57, 1, 1, 1),
(73, 5, 5, 58, 1, 1, 1),
(74, 6, 8, 59, 1, 1, 1),
(75, 5, 3, 60, 1, 1, 1),
(76, 6, 3, 61, 1, 1, 1),
(77, 5, 2, 17, 1, 1, 1),
(78, 2, 1, 62, 1, 1, 1),
(79, 8, 2, 0, 1, 1, 1),
(80, 1, 3, 0, 1, 1, 1),
(81, 2, 1, 63, 1, 1, 1),
(82, 5, 3, 64, 1, 1, 1),
(83, 5, 2, 65, 1, 1, 1),
(84, 5, 3, 64, 1, 1, 1),
(85, 5, 2, 66, 1, 1, 1),
(86, 1, 1, 15, 1, 1, 1),
(87, 5, 2, 67, 1, 1, 1),
(88, 2, 10, 68, 1, 1, 1),
(89, 5, 2, 69, 1, 1, 1),
(90, 2, 7, 70, 1, 1, 1),
(91, 5, 5, 71, 1, 1, 1),
(92, 5, 3, 72, 1, 1, 1),
(93, 5, 3, 73, 1, 1, 1),
(94, 5, 8, 74, 1, 1, 1),
(95, 5, 11, 75, 1, 1, 1),
(96, 5, 10, 76, 1, 1, 1),
(97, 2, 1, 40, 1, 1, 1),
(98, 5, 1, 77, 1, 1, 1),
(99, 5, 8, 78, 1, 1, 1),
(100, 5, 5, 7, 1, 1, 1),
(101, 5, 5, 79, 1, 1, 1),
(102, 2, 1, 40, 1, 1, 1),
(103, 5, 1, 80, 1, 1, 1),
(104, 5, 3, 32, 1, 1, 1),
(105, 1, 2, 81, 1, 1, 1),
(106, 2, 1, 40, 1, 1, 1),
(107, 2, 1, 40, 1, 1, 1),
(108, 5, 2, 82, 1, 1, 1),
(109, 6, 8, 83, 1, 1, 1),
(110, 5, 7, 84, 1, 1, 1),
(111, 5, 1, 85, 1, 1, 1),
(112, 0, 0, 0, 1, 1, 1),
(113, 1, 9, 86, 1, 1, 1),
(114, 5, 1, 1, 1, 1, 1),
(115, 5, 3, 87, 1, 1, 1),
(116, 5, 1, 88, 1, 1, 1),
(117, 5, 3, 89, 1, 1, 1),
(118, 5, 2, 90, 1, 1, 1),
(119, 7, 0, 0, 1, 1, 1),
(120, 5, 3, 91, 1, 1, 1),
(121, 5, 1, 92, 1, 1, 1),
(122, 5, 2, 69, 1, 1, 1),
(123, 5, 8, 93, 1, 1, 1),
(124, 5, 3, 32, 1, 1, 1),
(125, 2, 1, 94, 1, 1, 1),
(126, 5, 1, 95, 1, 1, 1),
(127, 2, 12, 96, 1, 1, 1),
(128, 1, 3, 97, 1, 1, 1),
(129, 5, 3, 98, 1, 1, 1),
(130, 5, 5, 99, 1, 1, 1),
(131, 5, 5, 100, 1, 1, 1),
(132, 2, 3, 101, 1, 1, 1),
(133, 8, 2, 0, 1, 1, 1),
(134, 5, 3, 102, 1, 1, 1),
(135, 5, 1, 103, 1, 1, 1),
(136, 5, 8, 104, 1, 1, 1),
(137, 7, 0, 0, 1, 1, 1),
(138, 5, 1, 21, 1, 1, 1),
(139, 0, 5, 105, 1, 1, 1),
(140, 5, 1, 77, 1, 1, 1),
(141, 5, 3, 32, 1, 1, 1),
(142, 5, 2, 106, 1, 1, 1),
(143, 5, 3, 51, 1, 1, 1),
(144, 5, 2, 107, 1, 1, 1),
(145, 5, 2, 108, 1, 1, 1),
(146, 5, 1, 109, 1, 1, 1),
(147, 2, 1, 110, 1, 1, 1),
(148, 5, 2, 34, 1, 1, 1),
(149, 5, 10, 43, 1, 1, 1),
(150, 2, 1, 110, 1, 1, 1),
(151, 5, 1, 111, 1, 1, 1),
(152, 5, 8, 112, 1, 1, 1),
(153, 5, 8, 113, 1, 1, 1),
(154, 6, 7, 114, 1, 1, 1),
(155, 5, 3, 115, 1, 1, 1),
(156, 2, 3, 116, 1, 1, 1),
(157, 5, 2, 117, 1, 1, 1),
(158, 6, 1, 118, 1, 1, 1),
(159, 9, 8, 104, 1, 1, 1),
(160, 2, 8, 119, 1, 1, 1),
(161, 6, 0, 0, 1, 1, 1),
(162, 5, 5, 120, 1, 1, 1),
(163, 5, 3, 64, 1, 1, 1),
(164, 5, 2, 121, 1, 1, 1),
(165, 1, 2, 122, 1, 1, 1),
(166, 5, 3, 87, 1, 1, 1),
(167, 5, 2, 123, 1, 1, 1),
(168, 5, 1, 109, 1, 1, 1),
(169, 2, 1, 124, 1, 1, 1),
(170, 2, 3, 18, 1, 1, 1),
(171, 5, 3, 125, 1, 1, 1),
(172, 5, 2, 126, 1, 1, 1),
(173, 2, 3, 4, 1, 1, 1),
(174, 5, 2, 127, 1, 1, 1),
(175, 5, 3, 102, 1, 1, 1),
(176, 1, 6, 128, 1, 1, 1),
(177, 5, 3, 129, 1, 1, 1),
(178, 2, 3, 130, 1, 1, 1),
(179, 2, 2, 131, 1, 1, 1),
(180, 8, 2, 132, 1, 1, 1),
(181, 2, 8, 133, 1, 1, 1),
(182, 5, 8, 134, 1, 1, 1),
(183, 1, 1, 135, 1, 1, 1),
(184, 5, 13, 136, 1, 1, 1),
(185, 4, 2, 137, 1, 1, 1),
(186, 5, 1, 138, 1, 1, 1),
(187, 3, 3, 130, 1, 1, 1),
(188, 5, 2, 10, 1, 1, 1),
(189, 5, 1, 42, 1, 1, 1),
(190, 9, 1, 0, 1, 1, 1),
(191, 5, 3, 139, 1, 1, 1),
(192, 5, 2, 140, 1, 1, 1),
(193, 5, 2, 82, 1, 1, 1),
(194, 5, 8, 141, 1, 1, 1),
(195, 4, 3, 142, 1, 1, 1),
(196, 5, 8, 143, 1, 1, 1),
(197, 2, 1, 144, 1, 1, 1),
(198, 5, 3, 91, 1, 1, 1),
(199, 5, 0, 120, 1, 1, 1),
(200, 2, 8, 145, 1, 1, 1),
(201, 5, 2, 127, 1, 1, 1),
(202, 5, 1, 146, 1, 1, 1),
(203, 5, 2, 140, 1, 1, 1),
(204, 6, 8, 147, 1, 1, 1),
(205, 5, 3, 32, 1, 1, 1),
(206, 5, 1, 148, 1, 1, 1),
(207, 5, 5, 149, 1, 1, 1),
(208, 5, 1, 103, 1, 1, 1),
(209, 6, 3, 150, 1, 1, 1),
(210, 2, 3, 116, 1, 1, 1),
(211, 5, 7, 151, 1, 1, 1),
(212, 5, 3, 152, 1, 1, 1),
(213, 4, 8, 153, 1, 1, 1),
(214, 6, 8, 154, 1, 1, 1),
(215, 2, 2, 155, 1, 1, 1),
(216, 6, 8, 156, 1, 1, 1),
(217, 2, 1, 157, 1, 1, 1),
(218, 5, 2, 158, 1, 1, 1),
(219, 2, 1, 49, 1, 1, 1),
(220, 0, 8, 156, 1, 1, 1),
(221, 6, 3, 159, 1, 1, 1),
(222, 5, 8, 160, 1, 1, 1),
(223, 5, 2, 161, 1, 1, 1),
(224, 2, 1, 56, 1, 1, 1),
(225, 2, 2, 162, 1, 1, 1),
(226, 2, 9, 163, 1, 1, 1),
(227, 5, 5, 105, 1, 1, 1),
(228, 6, 0, 0, 1, 1, 1),
(229, 2, 3, 130, 1, 1, 1),
(230, 2, 3, 164, 1, 1, 1),
(231, 5, 2, 165, 1, 1, 1),
(232, 5, 7, 166, 1, 1, 1),
(233, 5, 2, 167, 1, 1, 1),
(234, 5, 3, 51, 1, 1, 1),
(235, 5, 3, 125, 1, 1, 1),
(236, 5, 7, 84, 1, 1, 1),
(237, 9, 0, 0, 1, 1, 1),
(238, 5, 5, 168, 1, 1, 1),
(239, 5, 1, 169, 1, 1, 1),
(240, 5, 3, 170, 1, 1, 1),
(241, 5, 2, 171, 1, 1, 1),
(242, 2, 1, 172, 1, 1, 1),
(243, 3, 1, 173, 1, 1, 1),
(244, 3, 2, 0, 1, 1, 1),
(245, 7, 0, 0, 1, 1, 1),
(246, 5, 2, 174, 1, 1, 1),
(247, 5, 4, 175, 1, 1, 1),
(248, 5, 8, 176, 1, 1, 1),
(249, 5, 1, 80, 1, 1, 1),
(250, 5, 2, 69, 1, 1, 1),
(251, 5, 1, 177, 1, 1, 1),
(252, 2, 3, 116, 1, 1, 1),
(253, 7, 6, 0, 1, 1, 1),
(254, 0, 0, 0, 1, 1, 1),
(255, 3, 3, 130, 1, 1, 1),
(256, 10, 0, 0, 1, 1, 1),
(257, 3, 1, 178, 1, 1, 1),
(258, 5, 2, 165, 1, 1, 1),
(259, 5, 2, 179, 1, 1, 1),
(260, 5, 3, 73, 1, 1, 1),
(261, 1, 1, 180, 1, 1, 1),
(262, 5, 1, 103, 1, 1, 1),
(263, 5, 7, 84, 1, 1, 1),
(264, 5, 2, 181, 1, 1, 1),
(265, 2, 3, 116, 1, 1, 1),
(266, 5, 1, 182, 1, 1, 1),
(267, 5, 2, 179, 1, 1, 1),
(268, 5, 2, 183, 1, 1, 1),
(269, 2, 2, 184, 1, 1, 1),
(270, 5, 1, 21, 1, 1, 1),
(271, 5, 3, 32, 1, 1, 1),
(272, 5, 14, 185, 1, 1, 1),
(273, 5, 1, 186, 1, 1, 1),
(274, 1, 1, 187, 1, 1, 1),
(275, 2, 3, 18, 1, 1, 1),
(276, 5, 3, 188, 1, 1, 1),
(277, 5, 2, 17, 1, 1, 1),
(278, 8, 2, 0, 1, 1, 1),
(279, 5, 1, 85, 1, 1, 1),
(280, 2, 3, 130, 1, 1, 1),
(281, 5, 3, 51, 1, 1, 1),
(282, 3, 8, 189, 1, 1, 1),
(283, 2, 2, 190, 1, 1, 1),
(284, 7, 0, 0, 1, 1, 1),
(285, 7, 0, 0, 1, 1, 1),
(286, 2, 10, 191, 1, 1, 1),
(287, 5, 1, 109, 1, 1, 1),
(288, 5, 2, 192, 1, 1, 1),
(289, 1, 5, 193, 1, 1, 1),
(290, 6, 3, 194, 1, 1, 1),
(291, 2, 2, 195, 1, 1, 1),
(292, 5, 1, 85, 1, 1, 1),
(293, 5, 8, 196, 1, 1, 1),
(294, 5, 8, 196, 1, 1, 1),
(295, 5, 14, 197, 1, 1, 1),
(296, 5, 1, 80, 1, 1, 1),
(297, 2, 3, 116, 1, 1, 1),
(298, 5, 1, 198, 1, 1, 1),
(299, 5, 3, 64, 1, 1, 1),
(300, 2, 3, 130, 1, 1, 1),
(301, 2, 3, 130, 1, 1, 1),
(302, 5, 8, 199, 1, 1, 1),
(303, 5, 5, 200, 1, 1, 1),
(304, 5, 2, 3, 1, 1, 1),
(305, 5, 1, 85, 1, 1, 1),
(306, 5, 2, 140, 1, 1, 1),
(307, 5, 1, 21, 1, 1, 1),
(308, 1, 1, 25, 1, 1, 1),
(309, 1, 3, 201, 1, 1, 1),
(310, 5, 3, 102, 1, 1, 1),
(311, 5, 9, 202, 1, 1, 1),
(312, 2, 1, 203, 1, 1, 1),
(313, 5, 1, 169, 1, 1, 1),
(314, 5, 1, 177, 1, 1, 1),
(315, 5, 2, 204, 1, 1, 1),
(316, 5, 1, 103, 1, 1, 1),
(317, 5, 2, 205, 1, 1, 1),
(318, 5, 7, 206, 1, 1, 1),
(319, 5, 8, 141, 1, 1, 1),
(320, 8, 2, 207, 1, 1, 1),
(321, 1, 1, 180, 1, 1, 1),
(322, 5, 2, 179, 1, 1, 1),
(323, 0, 3, 0, 1, 1, 1),
(324, 5, 8, 208, 1, 1, 1),
(325, 5, 5, 120, 1, 1, 1),
(326, 5, 3, 209, 1, 1, 1),
(327, 2, 1, 178, 1, 1, 1),
(328, 2, 3, 210, 1, 1, 1),
(329, 5, 3, 211, 1, 1, 1),
(330, 2, 1, 49, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_perbaikan_estimasi`
--

CREATE TABLE `jenis_perbaikan_estimasi` (
  `estimasi_id` int(11) NOT NULL,
  `jenis_perbaikan_id` int(11) NOT NULL,
  `estimasi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jenis_perbaikan_estimasi`
--

INSERT INTO `jenis_perbaikan_estimasi` (`estimasi_id`, `jenis_perbaikan_id`, `estimasi`) VALUES
(1, 1, '300000'),
(2, 2, '75000'),
(3, 3, '150000'),
(4, 4, '0'),
(5, 5, '0'),
(6, 6, '75000'),
(7, 7, '0');

-- --------------------------------------------------------

--
-- Table structure for table `laptop_kelengkapan`
--

CREATE TABLE `laptop_kelengkapan` (
  `kelengkapan_id` int(11) NOT NULL,
  `kelengkapan` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laptop_kelengkapan`
--

INSERT INTO `laptop_kelengkapan` (`kelengkapan_id`, `kelengkapan`) VALUES
(1, 'Lengkap Tanpa Adaptor.'),
(2, 'Lengkap Tanpa Adaptor, hdd.'),
(3, 'Lengkap Tanpa Adaptor, Hdd, CDROM.'),
(4, 'Lengkap Tanpa Adaptor, Hdd, CDROM, Ram.');

-- --------------------------------------------------------

--
-- Table structure for table `laptop_kondisi_fisik`
--

CREATE TABLE `laptop_kondisi_fisik` (
  `kondisi_fisik_id` int(11) NOT NULL,
  `kondisi_fisik` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laptop_kondisi_fisik`
--

INSERT INTO `laptop_kondisi_fisik` (`kondisi_fisik_id`, `kondisi_fisik`) VALUES
(1, 'Mulus'),
(2, 'Engsel Kiri Rusak/Goyang'),
(3, 'Engsel Kanan Rusak/Goyang'),
(4, 'Engsel Kiri Kanan Rusak/Goyang'),
(5, 'Lcd/Layar Pecah'),
(6, 'Lcd/Layar Bergaris'),
(7, 'Casing Rusak');

-- --------------------------------------------------------

--
-- Table structure for table `laptop_merk`
--

CREATE TABLE `laptop_merk` (
  `merk_id` int(11) NOT NULL,
  `merk` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laptop_merk`
--

INSERT INTO `laptop_merk` (`merk_id`, `merk`) VALUES
(0, 'LAIN-LAIN'),
(1, 'Acer'),
(2, 'Lenovo'),
(3, 'Asus'),
(4, 'Samsung'),
(5, 'Toshiba'),
(6, 'Zyrex'),
(7, 'Dell'),
(8, 'HP'),
(9, 'Axioo'),
(10, 'Sony'),
(11, 'Gateway'),
(12, 'Apple'),
(13, 'Emachines'),
(14, 'Fujitsu');

-- --------------------------------------------------------

--
-- Table structure for table `laptop_model`
--

CREATE TABLE `laptop_model` (
  `model_id` int(11) NOT NULL,
  `merk_id` int(11) NOT NULL,
  `model_laptop` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laptop_model`
--

INSERT INTO `laptop_model` (`model_id`, `merk_id`, `model_laptop`) VALUES
(0, 0, 'LAIN-LAIN'),
(1, 1, '4738'),
(2, 1, '4732'),
(3, 2, 'G40-70'),
(4, 3, 'x200'),
(5, 2, 'ideapad 300'),
(6, 4, 'rv409'),
(7, 5, 'L745'),
(8, 1, 'E5-471'),
(9, 3, 'A442U'),
(10, 2, 'G400S'),
(11, 1, 'ACER ONE 10'),
(12, 7, 'SN'),
(13, 5, 'C40-A'),
(14, 7, 'D19M'),
(15, 1, 'V5-123'),
(16, 1, 'AO HAPPY PAV70'),
(17, 2, 'G40-45'),
(18, 3, 'E203M'),
(19, 5, 'L42'),
(20, 5, 'L735'),
(21, 1, 'ONE 14'),
(22, 3, 'X555B'),
(23, 5, 'C840'),
(24, 8, 'PROBOO K533 OM'),
(25, 1, 'D270'),
(26, 5, 'L840'),
(27, 2, '120S'),
(28, 1, 'ES1-111'),
(29, 1, 'ZE7'),
(30, 9, 'MY BOOK N10'),
(31, 5, 'E45W'),
(32, 3, 'X453M'),
(33, 5, 'L730'),
(34, 2, '130'),
(35, 1, 'Z14'),
(36, 3, 'X203NAH'),
(37, 1, 'ES1-420-3916'),
(38, 1, 'ZC-105'),
(39, 2, 'THINKPAD L412'),
(40, 1, 'E3-111'),
(41, 8, 'CPN-115'),
(42, 1, '4736'),
(43, 10, 'SVP132'),
(44, 1, 'N15W1'),
(45, 3, 'A407U'),
(46, 8, 'HP MINI 5103'),
(47, 8, 'CQ 40'),
(48, 1, '4925'),
(49, 1, 'ES1-131'),
(50, 1, 'Z1401'),
(51, 3, 'X441N'),
(52, 1, 'N16Q6'),
(53, 1, 'ZHG'),
(54, 8, 'TPN-c116'),
(55, 8, 'ELITE BOOK 850'),
(56, 1, 'V5-132'),
(57, 8, '242-G1'),
(58, 5, 'C640'),
(59, 8, 'DV6'),
(60, 3, 'X453S'),
(61, 3, 'S551L'),
(62, 1, 'N16Q9'),
(63, 1, 'P1VE6'),
(64, 3, 'X441B'),
(65, 2, '110-141BR'),
(66, 2, '110'),
(67, 2, 'IDEAPAD 320-14AST'),
(68, 10, 'SVE111A11W'),
(69, 2, 'G450'),
(70, 7, '62K7'),
(71, 5, 'M840'),
(72, 3, 'N43S'),
(73, 3, 'A407M'),
(74, 8, 'ELITE BOOK'),
(75, 11, 'MS2303'),
(76, 10, 'SVT131A11W'),
(77, 1, 'E1-431'),
(78, 8, 'RT3290'),
(79, 5, 'C300'),
(80, 1, 'Z1402'),
(81, 2, 'IDEAPAD'),
(82, 2, 'IDEAPAD 110'),
(83, 8, 'ELITEBOOK 850'),
(84, 7, 'INSPIRON 14'),
(85, 1, '4750'),
(86, 9, 'PJM-M1110'),
(87, 3, 'K45C'),
(88, 1, '4349'),
(89, 3, 'K43U'),
(90, 2, 'Y450'),
(91, 3, 'A455L'),
(92, 1, 'ES1-432'),
(93, 8, 'PAVILION G4'),
(94, 1, 'D255'),
(95, 1, 'E3-411'),
(96, 12, 'MAC AIR IOS'),
(97, 3, 'EeePc x101ch'),
(98, 3, 'A43S'),
(99, 5, 'M300'),
(100, 5, 'T420'),
(101, 3, 'X201'),
(102, 3, 'X441U'),
(103, 1, '4739'),
(104, 8, 'CQ 42'),
(105, 5, 'C600'),
(106, 2, '310'),
(107, 2, 'Z400'),
(108, 2, 'IDEAPAD S410P'),
(109, 1, 'E5-411'),
(110, 1, '725'),
(111, 1, 'V5-371'),
(112, 8, 'TPN-Q186'),
(113, 2, 'V310-14ISK'),
(114, 7, 'P12FOCS'),
(115, 3, 'X441M'),
(116, 3, 'E202S'),
(117, 2, '82C4'),
(118, 1, 'E5-552G'),
(119, 8, 'Q178'),
(120, 5, 'C800'),
(121, 2, 'G400'),
(122, 2, 's10-3'),
(123, 2, '80TQ'),
(124, 1, 'A311-31'),
(125, 3, 'X454Y'),
(126, 2, 'V14'),
(127, 2, 'IDEAPAD 320'),
(128, 6, 'M1110'),
(129, 3, 'X453'),
(130, 3, 'X200M'),
(131, 2, 'S210'),
(132, 2, 'F0B3'),
(133, 8, 'TPN-Q164'),
(134, 8, 'HSTNN-Q72C'),
(135, 1, 'D257'),
(136, 13, 'D720'),
(137, 2, '3680'),
(138, 1, 'V3-471'),
(139, 3, 'X452E'),
(140, 2, 'G40'),
(141, 8, 'CQ 43'),
(142, 3, 'A411Q'),
(143, 8, 'TPN-Q187'),
(144, 1, 'V5-131'),
(145, 8, 'TPN-Q166'),
(146, 1, 'A3141'),
(147, 8, 'TPN-Q162'),
(148, 1, '4352'),
(149, 5, 'L40-A'),
(150, 3, 'X550D'),
(151, 7, 'N4101'),
(152, 3, 'A456U'),
(153, 8, 'TM2'),
(154, 8, 'RTL8723BE'),
(155, 2, '300S'),
(156, 8, 'TPN-Q175'),
(157, 1, 'ES1-132'),
(158, 2, 'L420'),
(159, 3, 'GL503V'),
(160, 8, 'RMN'),
(161, 2, '80Q3'),
(162, 2, 'S130'),
(163, 9, 'CW11Q3'),
(164, 3, 'Eeeepc 1215'),
(165, 2, 'G40-30'),
(166, 7, 'N4050'),
(167, 2, 'U41'),
(168, 5, 'L850'),
(169, 1, '4253'),
(170, 3, 'S400C'),
(171, 2, '320'),
(172, 1, 'R3-131T'),
(173, 1, 'ASPIRE E11'),
(174, 2, 'V130-14IGM'),
(175, 4, 'NP275E4V'),
(176, 8, '431'),
(177, 1, 'V5-431'),
(178, 1, 'E3-112'),
(179, 2, '330'),
(180, 1, 'NAV50'),
(181, 2, 'B40-70'),
(182, 1, 'E5-473G'),
(183, 2, 'T520'),
(184, 2, 'IDEAPAD 310S'),
(185, 14, 'ELITEBOOK'),
(186, 1, '4730Z'),
(187, 1, 'AO 722'),
(188, 3, 'A470U'),
(189, 8, 'TPN-C115'),
(190, 2, 'YOGA'),
(191, 10, 'VGN-TZ17GN'),
(192, 2, 'THINKPAD E450'),
(193, 5, 'NB520'),
(194, 3, 'X540L'),
(195, 2, 'S310'),
(196, 8, 'MODEL 14-CM0071AU'),
(197, 14, 'LIFEBOOK E449'),
(198, 1, 'N17Q4'),
(199, 8, '14-CKOO11TU'),
(200, 5, 'C40-B'),
(201, 3, '1015E'),
(202, 9, 'NEON BNE'),
(203, 1, 'V5-121'),
(204, 2, '409F'),
(205, 2, 'G410'),
(206, 7, 'LATITUDE E6330'),
(207, 2, '10114'),
(208, 8, 'PAVILION G4-1212TX'),
(209, 3, 'X455L'),
(210, 3, 'E203'),
(211, 3, 'X441S');

-- --------------------------------------------------------

--
-- Table structure for table `laptop_ukuran`
--

CREATE TABLE `laptop_ukuran` (
  `ukuran_id` int(11) NOT NULL,
  `ukuran` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laptop_ukuran`
--

INSERT INTO `laptop_ukuran` (`ukuran_id`, `ukuran`) VALUES
(0, 'Lain-lain'),
(1, 'Laptop 10\"'),
(2, 'Laptop 11\"'),
(3, 'Laptop 12\"'),
(4, 'Laptop 13\"'),
(5, 'Laptop 14\"'),
(6, 'Laptop 15\"'),
(7, 'Komputer PC'),
(8, 'All In One'),
(9, 'Motherboard saja'),
(10, 'Harddisk');

-- --------------------------------------------------------

--
-- Table structure for table `laptop_warna`
--

CREATE TABLE `laptop_warna` (
  `warna_id` int(11) NOT NULL,
  `warna` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laptop_warna`
--

INSERT INTO `laptop_warna` (`warna_id`, `warna`) VALUES
(1, 'Hitam'),
(2, 'Putih'),
(3, 'Silver'),
(4, 'Gold'),
(5, 'Merah'),
(6, 'Pink'),
(7, 'Kuning'),
(8, 'Hijau');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `nama` varchar(120) NOT NULL,
  `jabatan` varchar(120) NOT NULL,
  `no_hp` varchar(120) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `role_id` int(2) NOT NULL,
  `date_created` int(11) NOT NULL,
  `soft_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `nama`, `jabatan`, `no_hp`, `username`, `password`, `foto`, `role_id`, `date_created`, `soft_delete`) VALUES
(60, 'ince', 'Teknisi', '221082345678918', 'ince', '$2y$10$dHHAP1RohrxHJTSTq62CsOAaRij92ByVtYgnoVWM5nGGjLxSak72O', '5fedbacb393a9.jpg', 2, 1609415371, 1),
(61, 'admin', 'Teknisi', '08321832649132', 'admin', '$2y$10$iHw6dkAxc91QgRwa5OczYusCUQvyX.glNDuKpYMvFC2yhf2Mqqrfi', '5fedbda5d528f.jpg', 1, 1609416101, 1),
(62, 'israr', 'Teknisi', '08238184918931', 'israr', '$2y$10$JFrlXHj0LJoOkWyBNaLWbOo6hWA26gsz8KLv9hJtponSmvYViGp6S', '5fedc2091cd27.jpg', 2, 1609417225, 1),
(63, 'fatur', 'Teknisi', '0921291841837', 'fatur', '$2y$10$pYE9de7mTbJYQ1PewgVdmO2mj2Xu/SL0j40CM4JpAONek/JJfUmpW', '5fedc21aa161f.jpg', 2, 1609417242, 1),
(64, 'hamkah', 'Teknisi', '0853716339847', 'hamkah', '$2y$10$FMdkOs19Qsfx5BvXiE.U6uZn8jNfFBbO2rMauuRn4J4BjrunSZfhe', '5fedc22f629be.jpg', 2, 1609417263, 1),
(65, 'arsal', 'Teknisi', '0982379092738', 'arsal', '$2y$10$5vH/Qz0akS9piF9qINaOr.B/18CEWyXI5kk3TTMVUJB1WMIAAJ.KC', '5fedc24fc1acb.jpg', 2, 1609417295, 1),
(66, 'syarifuddin', 'Teknisi', '0879863498729', 'syarifuddin', '$2y$10$ZZizpbNuip/Lk5YOk4SlX.k0wyHBPOViUutwC3MaV2LvtVzckFZPG', '5fedc26b1e757.jpg', 2, 1609417323, 1),
(67, 'asrul', 'Teknisi', '0879869909798', 'asrul', '$2y$10$.0Da5taIz8D8sbfe982RpOIzZ3AP65G6BUM8.cw/XQbdwda/O7Jpa', '5fedc2a140c61.jpg', 2, 1609417377, 1),
(68, 'fitrah', 'Teknisi', '09723692323', 'fitrah', '$2y$10$1xLu8VwXNM5DpQNPgPDmYea6gpnTcunOIiuXmQdSVC3cjA3B.pIzK', '5fedc2afe5929.jpg', 2, 1609417392, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `user_access_menu_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`user_access_menu_id`, `role_id`, `user_menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 2, 2),
(6, 2, 3),
(7, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `user_menu_id` int(11) NOT NULL,
  `menu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`user_menu_id`, `menu`) VALUES
(1, 'TEKNISI'),
(2, 'COSTUMER'),
(3, 'SERVISAN'),
(4, 'LOGOUT');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`role_id`, `role`) VALUES
(1, 'Manager'),
(2, 'Teknisi');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `user_sub_menu_id` int(11) NOT NULL,
  `user_menu_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`user_sub_menu_id`, `user_menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(6, 2, 'Tanda Terima', 'Costumer/dataCostumer', 'fas fa-address-book', 1),
(7, 1, 'Data Teknisi', 'KelolaTeknisi/index', 'fas fa-wrench', 1),
(8, 4, 'Logout', 'Auth/logout', 'fas fa-sign-out-alt', 1),
(9, 3, 'Belum Dikerja', 'Servisan/belumDikerja', 'fas fa-list', 1),
(10, 3, 'Proses Pengerjaan', 'Servisan/prosesPengerjaan', 'fa fa-spinner', 1),
(11, 3, 'Sevisan selesai', 'Servisan/servisanSelesai', 'fas fa-check', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `costumer`
--
ALTER TABLE `costumer`
  ADD PRIMARY KEY (`costumer_id`);

--
-- Indexes for table `costumer_jenis_perbaikan`
--
ALTER TABLE `costumer_jenis_perbaikan`
  ADD PRIMARY KEY (`jenis_perbaikan_id`);

--
-- Indexes for table `costumer_laptop`
--
ALTER TABLE `costumer_laptop`
  ADD PRIMARY KEY (`laptop_id`);

--
-- Indexes for table `jenis_perbaikan_estimasi`
--
ALTER TABLE `jenis_perbaikan_estimasi`
  ADD PRIMARY KEY (`estimasi_id`);

--
-- Indexes for table `laptop_kelengkapan`
--
ALTER TABLE `laptop_kelengkapan`
  ADD PRIMARY KEY (`kelengkapan_id`);

--
-- Indexes for table `laptop_kondisi_fisik`
--
ALTER TABLE `laptop_kondisi_fisik`
  ADD PRIMARY KEY (`kondisi_fisik_id`);

--
-- Indexes for table `laptop_merk`
--
ALTER TABLE `laptop_merk`
  ADD PRIMARY KEY (`merk_id`);

--
-- Indexes for table `laptop_model`
--
ALTER TABLE `laptop_model`
  ADD PRIMARY KEY (`model_id`);

--
-- Indexes for table `laptop_ukuran`
--
ALTER TABLE `laptop_ukuran`
  ADD PRIMARY KEY (`ukuran_id`);

--
-- Indexes for table `laptop_warna`
--
ALTER TABLE `laptop_warna`
  ADD PRIMARY KEY (`warna_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`user_access_menu_id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`user_menu_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`user_sub_menu_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `costumer`
--
ALTER TABLE `costumer`
  MODIFY `costumer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1061;

--
-- AUTO_INCREMENT for table `costumer_jenis_perbaikan`
--
ALTER TABLE `costumer_jenis_perbaikan`
  MODIFY `jenis_perbaikan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `costumer_laptop`
--
ALTER TABLE `costumer_laptop`
  MODIFY `laptop_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=331;

--
-- AUTO_INCREMENT for table `jenis_perbaikan_estimasi`
--
ALTER TABLE `jenis_perbaikan_estimasi`
  MODIFY `estimasi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `laptop_kelengkapan`
--
ALTER TABLE `laptop_kelengkapan`
  MODIFY `kelengkapan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `laptop_kondisi_fisik`
--
ALTER TABLE `laptop_kondisi_fisik`
  MODIFY `kondisi_fisik_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `laptop_merk`
--
ALTER TABLE `laptop_merk`
  MODIFY `merk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `laptop_model`
--
ALTER TABLE `laptop_model`
  MODIFY `model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;

--
-- AUTO_INCREMENT for table `laptop_ukuran`
--
ALTER TABLE `laptop_ukuran`
  MODIFY `ukuran_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `laptop_warna`
--
ALTER TABLE `laptop_warna`
  MODIFY `warna_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `user_access_menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `user_menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `user_sub_menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
